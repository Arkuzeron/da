package dungeonadventures;

import jplay.Window;
import jplay.Sprite;
import jplay.Keyboard;
import jplay.Sound;

public class Menu{
    private int escolha = 0;
    private short som = 0;
    private Music mus = new Music();


    public Menu(Window janela, Sprite menu, Keyboard teclado, String ms){
        carregarObjetos(janela,menu,teclado, ms);
        atualizarEstado(2,menu,teclado, janela);

        //descarregarObjetos();
    }

    protected void carregarObjetos(Window janela, Sprite menu, Keyboard teclado, String ms){
        this.mus.play("menu/musicaMenu.wav");
        janela.setCursor(janela.createCustomCursor(" "));
        menu = new Sprite("menu/spriteMenuFinal.png",3);
        teclado = janela.getKeyboard();
        teclado.setBehavior(Keyboard.UP_KEY,   Keyboard.DETECT_INITIAL_PRESS_ONLY);
        teclado.setBehavior(Keyboard.DOWN_KEY, Keyboard.DETECT_INITIAL_PRESS_ONLY);

        //musica = new Sound("menu/musicaMenu.wav");
        //musica.setRepeat(true);

    }



    protected void desenhar(Sprite menu,Window janela){
        menu.draw();
        janela.update();
    }

    protected void atualizarEstado(int n, Sprite menu, Keyboard teclado, Window janela){
        while(true){
            boolean escolheuUmaOpcao = false;

            if(teclado.keyDown(Keyboard.UP_KEY) && escolha > 0){
                escolheuUmaOpcao = true;
                escolha--;
            }
            if(teclado.keyDown(Keyboard.DOWN_KEY) && escolha < n){
                escolheuUmaOpcao = true;
                escolha++;
            }

            if(teclado.keyDown(Keyboard.ENTER_KEY)){
                if(escolha == 0){
                    teclado.setBehavior(Keyboard.UP_KEY,   Keyboard.DETECT_EVERY_PRESS);
                    teclado.setBehavior(Keyboard.DOWN_KEY, Keyboard.DETECT_EVERY_PRESS);
                    Stage st = new Stage(janela);
                }
                if(escolha == 1){
                    Options op = new Options(som, "menu/spriteOpcoesFinal.png", teclado, janela, this.mus);

                }
                if(escolha == 2){
                    janela.exit();
                }
            }

            if(escolheuUmaOpcao){
                new Sound("menu/som.wav").play();
            }

            menu.setCurrFrame(escolha);
            this.desenhar(menu, janela);
            janela.display();
        }
    }

    protected void escolhaEstado(Window janela, Keyboard teclado){

        if(escolha == 0){
            if(teclado.keyDown(Keyboard.ENTER_KEY)){
                new Sound("menu/som.wav").play();
                Stage jogo = new Stage(janela);
            }
        }if(escolha == 1){
            if(teclado.keyDown(Keyboard.ENTER_KEY)){
                new Sound("menu/som.wav").play();
                //Opcoes jogo = new Opcoes(janela);
                //descarregarObjetos();
            }
        }if(escolha == 2){
            if(teclado.keyDown(Keyboard.ENTER_KEY)){
                new Sound("menu/som.wav").play();
                janela.exit();
            }
        }
    }

    //protected void descarregarObjetos(){
        //janela.exit();
        //janela = null;
        //menu = null;
        //teclado = null;
    //}
}