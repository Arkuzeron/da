/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dungeonadventures;

import jplay.Keyboard;
import jplay.Sprite;
import jplay.Window;

/**
 *
 * @author Eduardo
 */
public class Options {
    private short som;
    private short escolha = 0;
    private Keyboard teclado;
    private Window janela;
    private Sprite fundo;
    private Music mus;

    public Options(int sm, String sp, Keyboard tec, Window jan, Music ms){
        this.som = (short)sm;
        this.teclado = tec;
        this.janela = jan;
        this.fundo = new Sprite("menu/spriteOpcoesFinal.png", 4);

        this.run();

    }

    public void run(){
        boolean ret = true;
        boolean detect;
        while(ret){
            detect = this.teclado.keyDown(Keyboard.ENTER_KEY);
            if(detect && this.escolha == 0){
                if(this.som == 0){
                    this.som = 1;
                    this.mus.stop();
                }else{
                    this.som = 0;
                    this.mus.play("menu/musicaMenu.wav");
                }
            }
            if(detect && this.escolha == 1){
                ret = false;
            }

            if(this.teclado.keyDown(Keyboard.DOWN_KEY) && escolha == 0){
                escolha++;
            }
            if(this.teclado.keyDown(Keyboard.UP_KEY) && escolha == 1){
                escolha--;
            }

            this.fundo.setCurrFrame(this.escolha*2+this.som);
            this.fundo.draw();
            this.janela.display();


        }
    }


}