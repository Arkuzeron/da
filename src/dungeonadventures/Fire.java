
package dungeonadventures;

import jplay.Sprite;

public class Fire extends Sprite{
    public static final int LEFT = 1, RIGHT = 2, STOP = 3, UP = 4, DOWN = 5;

    protected static final int VELOCIDADE_TIRO = 7;
    protected int caminho = STOP;
    protected boolean movendo = false;
    protected int direcao = 3;

    public Fire(double x, double y, int caminho){
        super("player/fire.png",12);
        this.caminho = caminho;
        this.x = x;
        this.y = y;

    }

    public void mover(){
        if(caminho == LEFT){
            this.x -= VELOCIDADE_TIRO;
            if(direcao != 1){
                setSequence(0,2);
            }
            movendo = true;
        }
        if(caminho == RIGHT){
            this.x += VELOCIDADE_TIRO;
            if(direcao != 2){
                setSequence(3,5);
            }
            movendo = true;
        }
        if(caminho == UP){
            this.y -= VELOCIDADE_TIRO;
            if(direcao != 4){
                setSequence(6,8);
            }
            movendo = true;
        }
        if(caminho == DOWN || caminho == STOP){
            this.y += VELOCIDADE_TIRO;
            if(direcao != 5){
                setSequence(9,11);
            }
            movendo = true;
        }
        if(movendo){
            update();
            movendo = false;
        }
    }

}
