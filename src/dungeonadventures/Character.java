
package dungeonadventures;

import jplay.GameObject;
import jplay.Scene;
import jplay.TileInfo;
import java.util.Vector;
import java.awt.Point;
import jplay.Sprite;

public class Character extends Sprite{

    protected double speed = 1;
    protected int direcao = 3;
    protected boolean movendo = false;
    Control control = new Control();
    public double life = 3;

    public Character(String fileName, int numFrames){
        super(fileName, numFrames);
    }


    public void way(Scene scene){
        Point min = new Point((int)this.x,(int)this.y);
        Point max = new Point((int)this.x + this.width,(int)this.y + this.height);

        Vector<?>tiles = scene.getTilesFromPosition(min,max);

        for(int i = 0; i < tiles.size(); i++){
            TileInfo tile = (TileInfo) tiles.elementAt(i);

            if(control.colisao(this, tile) == true){

                if(colisaoVertical(this, tile)){

                    if(tile.y + tile.height - 2 < this.y){
                        this.y = tile.y + tile.height;
                    }
                    else if(tile.y > this.y + this.height - 2){
                        this.y = tile.y - this.height;
                    }
                }
                if(colisaoHorizontal(this, tile)){
                    if(tile.x > this.x + this.width - 2){
                        this.x = tile.x - this.width;
                    }else{
                        this.x = tile.x + tile.width;
                    }

                }
            }
        }

    }

    private boolean colisaoVertical(GameObject obj, GameObject obj2){
        if(obj2.x + obj2.width <= obj.x || obj.x + obj.width <= obj2.x)
            return false;
        return true;
    }

    private boolean colisaoHorizontal(GameObject obj, GameObject obj2){
        if(obj2.y + obj2.height <= obj.y || obj.y + obj.height <= obj2.y)
            return false;
        return true;
    }


}
