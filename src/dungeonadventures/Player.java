/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dungeonadventures;

import jplay.Sprite;
import jplay.Keyboard;
import jplay.Window;
import jplay.Scene;
import java.awt.event.KeyEvent;
import java.awt.Font;
import java.awt.Color;
import java.util.ArrayList;


public class Player extends Character{

    private Keyboard teclado;
    public static double life = 3;
    private ControlFire fires = new ControlFire();
    private ArrayList<Sprite> heart = new ArrayList<Sprite>();


    public Player(int x, int y){
        super("player/PLAYER_MOVIMENTO2.png",36);
        for(int i = 0; i < this.life; i++){
            heart.add(new Sprite("menu/life.jpg"));
            heart.get(i).x = i*(20+heart.get(i).width);
        }
        this.x = x;
        this.y = y;
        this.setTotalDuration(2000);

    }
    
    public void removeHeart(){
        this.heart.remove(this.heart.size()-1);
    }

    public void shoot(Window janela, Scene cena, Keyboard teclado, ArrayList<Enemie> enm){

        if(teclado.keyDown(Keyboard.A_KEY)){
            fires.addFire(x, y, direcao, cena);
        }
        fires.run(enm, janela, cena);

    }


    public void mover(Window janela){
        if(teclado == null){
            teclado = janela.getKeyboard();
        }
        if(teclado.keyDown(Keyboard.LEFT_KEY)){
            if(this.x > 0)
                this.x -= speed;
            if(direcao != 1){
                this.setSequence(10,18);
                direcao = 1;
            }movendo = true;

        }else if(teclado.keyDown(Keyboard.RIGHT_KEY)){
            if(this.x < janela.getWidth() - 50)
                this.x += speed;
            if(direcao != 2){
                this.setSequence(19,27);
                direcao = 2;
            }movendo = true;

        }else if(teclado.keyDown(Keyboard.UP_KEY)){
            if(this.y > 0)
                this.y -= speed;
            if(direcao != 4){
                this.setSequence(28,36);
                direcao = 4;
            }movendo = true;

        }else if(teclado.keyDown(Keyboard.DOWN_KEY)){
            if(this.y < janela.getHeight() - 50)
                this.y += speed;
            if(direcao != 5){
                this.setSequence(0,9);
                direcao = 5;
            }movendo = true;

        }

        if(movendo){
            update();
            movendo = false;
        }


    }

Font f = new Font("Arial", Font.BOLD,30);

public void life(Window janela){
    for(int i = 0; i < this.heart.size(); i++){
        heart.get(i).draw();
    }
}


}

