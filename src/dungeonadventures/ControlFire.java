
package dungeonadventures;

import java.util.ArrayList;
import java.util.LinkedList;
import jplay.Scene;
import jplay.Sound;
import jplay.Window;

public class ControlFire {

    LinkedList<Fire> fires = new LinkedList<Fire>();
    
    private boolean outScreen(Fire fr, Window janela){
        if(fr.x > janela.getWidth() || fr.x + fr.width < 0 || fr.y > janela.getHeight() || fr.y + fr.height < 0){
            return true;
        }
        return false;
    }
    
    
    public void addFire(double x, double y, int caminho, Scene cena){
        Fire fire = new Fire(x,y,caminho);
        fires.add(fire);
        cena.addOverlay(fire);
        soundFire();

    }

    
    public void run(ArrayList<Enemie> enemies, Window janela, Scene scn){
        boolean coll;
        for(int i = 0; i < fires.size(); i++){
            coll = false;
            Fire fr = fires.remove();
            fr.mover();
            if(this.outScreen(fr, janela)){
                coll = true;
            }
            for(int j = 0; j < enemies.size(); j++){
                if(fr.collided(enemies.get(j))){
                    enemies.get(j).life -= 1;
                    if(enemies.get(j).dead()){
                        enemies.remove(j);
                    }
                    scn.removeOverlay(fr);
                    coll = true;
                }
                
            }
            if(coll != true){
                this.fires.add(fr);
            }
            
        }

    }

    private void soundFire(){
        new Sound("player/fire.wav").play();
    }

}
