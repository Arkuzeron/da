
package dungeonadventures;

import java.util.ArrayList;
import jplay.Window;
import jplay.Scene;
import jplay.Keyboard;


public class Stage {

    private Window janela;
    private Scene cena;
    private Player player;
    private Keyboard teclado;
    private ArrayList<Enemie> enemies;
    private ArrayList<Bonus> bonuses;

    public Stage(Window window){

        janela = window;
        cena = new Scene();
        cena.loadFromFile("cenario.txt");
        player = new Player(20,20);
        teclado = janela.getKeyboard();
        enemies = new ArrayList<Enemie>();
        bonuses = new ArrayList<Bonus>();



        run();
    }

    private void run(){

        enemies.add(new Enemie(500 , 500));
        enemies.add(new Enemie(550 , 550));
        enemies.add(new Enemie(600 , 300));
        enemies.add(new Enemie(600 , 230));
        enemies.add(new Enemie(100 , 500));
        enemies.add(new Enemie(500 , 500));
        
        bonuses.add(new Bonus(400,300));
        
        boolean executando = true;
        while(executando){
            //cena.draw();
            player.mover(janela);
            player.way(cena);

            cena.moveScene(player);

            player.x += cena.getXOffset();
            player.y += cena.getYOffset();
            player.draw();
            player.shoot(janela,cena,teclado, enemies);

            for(int i = 0; i < enemies.size(); i++){
                enemies.get(i).way(cena);
                enemies.get(i).follow(player.x,player.y);
                enemies.get(i).x += cena.getXOffset();
                enemies.get(i).y += cena.getYOffset();
                enemies.get(i).draw();

                enemies.get(i).atk(player);

            }
            
            for(int i = 0; i < bonuses.size(); i++){
                bonuses.get(i).draw();
            }


            player.life(janela);

            janela.delay(4);
            janela.update();
            
        }
    }


}
