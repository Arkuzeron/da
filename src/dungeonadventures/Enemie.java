/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dungeonadventures;

/**
 *
 * @author Gabriel
 */
public class Enemie extends Character {

    private double atk = 1;

    public Enemie(int x, int y){
        super("enemie/enemie.png", 12);
        this.x = x;
        this.y = y;
        this.setTotalDuration(2000);
        this.speed = 0.6;
    }

    public void follow(double x, double y){
        if(this.x > x && this.y <= y + 50 && this.y >= y - 50){
            moveTo(x,y,speed);
            if(direcao != 1){
                setSequence(0,1);
                direcao = 1;
            }
            movendo = true;
        }
        else if(this.x < x && this.y <= y + 50 && this.y >= - 50){
            moveTo(x,y,speed);
            if(direcao != 2){
                setSequence(2,3);
                direcao = 2;
            }
            movendo = true;
        }

        else if(this.y > y){
            moveTo(x,y,speed);
            if(direcao != 4){
                setSequence(4,6);
                direcao = 4;
            }
            movendo = true;
        }
        else if(this.y < y){ //pra baixo
            moveTo(x,y,speed);
            if(direcao != 5){
                setSequence(7,11);
                direcao = 5;
            }
            movendo = true;
        }
        if(movendo){
            update();
            movendo = false;
        }
    }

    public boolean dead(){
        if(this.life <= 0){
            return true;
        }
        return false;
    }
    
    public void atk(Player player){
        if(this.collided(player)){
            player.x = 20;
            player.y = 20;
            Player.life -= this.atk;
            player.removeHeart();
        }
        
        if(Player.life <= 0){
            System.exit(0);
        }
        
        
    }



}
