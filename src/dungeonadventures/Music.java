
package dungeonadventures;

import jplay.Sound;

public class Music {

    private static Sound musica;

    public static void play(String audio){
        stop();
        musica = new Sound((audio));
        Music.musica.play();
        Music.musica.setRepeat(true);

    }

    public static void stop(){
        if(Music.musica != null){
            musica.stop();
        }
    }

}
